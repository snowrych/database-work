#include "DataLayer/FileManager.h"
#include <QQmlEngine>

FileManager::FileManager()
{

}

QString FileManager::filePath() const
{
    return m_filePath;
}

void FileManager::setFilePath(const QString &filePath)
{
    m_filePath = filePath;
    emit filePathChanged();
}

QList<QObject*> FileManager::userDataList() const
{
    return m_userList;
}

void FileManager::readFile()
{
    showWaitCursor("Чтение файла");
    //QString filename = QFileDialog::getOpenFileName(nullptr,tr("Чтение файла"),
     //                                            QDir::homePath()+ "/Desktop",tr("Текстовые файлы (*.txt);;Другие файлы (*.*)"));
    // local example file
    QString filename =   ":/examples/users.txt";
    setFilePath(filename);
    getFileData();

    /* test unit
     QEventLoop loop;
     QTimer::singleShot(5000,&loop,SLOT(quit()));
     loop.exec();

     m_userList.append(new UserPm(0,"Gena","888888888"));
     m_userList.append(new UserPm(1,"Test","777777777"));
// cheking data in list
   for(auto obj : m_userList) {
         UserPm *p = (UserPm *)obj; //Remeber using cast like this c style
         qDebug() <<"userID " <<  p->userId();
         qDebug() <<"name " << p->name();
         qDebug() <<"phone " << p->phone();
     }
    //  can make signal readyRead(m_userList); but no need (no Vm)
*/
    hideWaitCursor();

    emit userListChanged();
}

void FileManager::getFileData()
{
    QFile file(filePath());
    QString line;
    qDebug() << "selected file: " + filePath();
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        qDebug() << "error open file";
    }else{

        QTextStream stream(&file);
            while (!stream.atEnd()){
                line = stream.readLine();
                QStringList records = line.split(QLatin1Char(';'), Qt::SkipEmptyParts);
               m_userList.append(new UserPm(records));
            }
    }
    file.close();
}
void FileManager::clearList()
{
    m_userList.clear();
    emit userListChanged();
}

QString FileManager::busyText() const
{
    return m_busyText;
}

void FileManager::setBusyText(const QString &busyText)
{
    m_busyText = busyText;
    emit busyTextChanged();
}
void FileManager::showWaitCursor(const QString &text)
{
    setBusyText(text);
}

void FileManager::hideWaitCursor()
{
    setBusyText("");
}

QList<QObject *> FileManager::userList() const
{
    return m_userList;
}

void FileManager::setUserList(const QList<QObject *> &userList)
{
    m_userList = userList;
}
// TODO appending here objects to list

