#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <QObject>
#include <QString>
#include <QDir>
#include <QFileDialog>

//#include <QTimer>
//#include <QEventLoop>

#include "ViewModels/UserPm.h"
#include "Core/Macros.h"

class FileManager  : public QObject {
    Q_OBJECT
    Q_PROPERTY(QString filePath READ filePath WRITE setFilePath NOTIFY filePathChanged)
    Q_PROPERTY(QString busyText READ busyText WRITE setBusyText NOTIFY busyTextChanged)
    Q_PROPERTY(QList<QObject*> newList READ userList NOTIFY userListChanged )


public:
    FileManager();
        SINGLETON(FileManager)

    QString filePath() const;
    void setFilePath(const QString &filePath);

    QList<QObject*> userDataList() const;
    //    void setUserList(QObject &userRecord);
    QList<QObject *> userList() const;
    void setUserList(const QList<QObject *> &userList);

    QString busyText() const;
    void setBusyText(const QString &busyText);

    void getFileData();

    void showWaitCursor(const QString& text);
    void hideWaitCursor();
public slots:
    void readFile();
    void clearList();
signals:
    void filePathChanged();
    void userListChanged();
    void busyTextChanged();
//    void readyRead(const  QList<QObject*> &listValue);
// void readyRead();
private:
    QString m_filePath="";
    QString m_busyText="";
    QList<QObject*> m_userList;
};

#endif // FILEMANAGER_H
