import QtQuick 2.0
import QtQuick.Controls 2.14

Rectangle {
    id:item
    radius: 2
    anchors.fill: parent
    Row {
        spacing: 2
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        Text { anchors.verticalCenter: parent.verticalCenter;text: qsTr("id: "+modelData.userId) }
        Text { anchors.verticalCenter: parent.verticalCenter;text: qsTr("name: "+modelData.name) }
        Text { anchors.verticalCenter: parent.verticalCenter;text: qsTr("phone: "+modelData.phone ) }
    }
    width: parent.width /2
    height: 25
    border.color: "black"
    border.width: 0.5
    MouseArea {
        id:dragArea
        anchors.fill: parent

        cursorShape : Qt.ClosedHandCursor
        hoverEnabled: true
        onEntered:  item.color = "#006666"
        onExited:item.color ="transparent"
    }
}
