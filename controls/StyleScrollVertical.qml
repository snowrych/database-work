import QtQuick 2.0
import QtQuick.Controls 2.14

ScrollBar {
    id: control1
    policy: ScrollBar.AsNeeded
    contentItem: Rectangle {
        implicitWidth: 5
        radius: 3
        color: control1.hovered ? secondaryColor :primaryColor
        border.color: borderColor
        border.width: 0.5
    }
}
