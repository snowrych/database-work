import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.14

MenuBar {
    font.family: fontFamily
    font.pixelSize: fontPixelSize
    Menu {
        font.family: parent.font.family
        font.pixelSize: parent.font.pixelSize
        delegate: MenuItem {
            id: menuItem
            implicitWidth: 200
            implicitHeight: fontPixelSize > maxFontPixelSize ? barMaxHeight : barMinHeight
            contentItem:/* Row{
                spacing: 2
                Image{
                    sourceSize: Qt.size(20,20)
                    anchors.verticalCenter: parent.verticalCenter
                    source: {
                        switch(textDelegate.text) {
                        case "Открыть" : return "qrc:/IconSource/open.png"
                        case "Сохранить" : return "qrc:/IconSource/save.png"
                        case "Выход" : return "qrc:/IconSource/exit.png"
                        }
                    }
                }*/

                        Text {
                text: menuItem.text
                font.family: parent.font.family
                font.pixelSize: parent.font.pixelSize
                color: menuItem.highlighted ? "#ffffff" : textColor
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
                //                anchors.verticalCenter: parent.verticalCenter
                //                }
            }

            background: Rectangle {
                implicitWidth: 100
                implicitHeight: fontPixelSize > maxFontPixelSize ? barMaxHeight : barMinHeight
                color: menuItem.highlighted ? secondaryColor : "transparent"
            }
        }
        title: qsTr("Файл")

        Action { text: qsTr("Открыть");onTriggered: {fileManager.clearList(); fileManager.readFile()} }
        Action { text: qsTr("Сохранить");enabled: false}
        MenuSeparator {
            contentItem: Rectangle {
                implicitHeight: 1
                color: secondaryColor
            }
        }
        Action { text: qsTr("Выход");onTriggered: Qt.quit() }
    }

    Menu{
        font.family: parent.font.family
        font.pixelSize: parent.font.pixelSize
        delegate: MenuItem {
            id: menuItem2
            implicitWidth: 200
            implicitHeight: fontPixelSize > maxFontPixelSize ? barMaxHeight : barMinHeight
            contentItem:/* Row{
                spacing: 2
                Image{
                    sourceSize: Qt.size(20,20)
                    anchors.verticalCenter: parent.verticalCenter
                    source: {
                        switch(textDelegate.text) {
                        case "Открыть" : return "qrc:/IconSource/open.png"
                        case "Сохранить" : return "qrc:/IconSource/save.png"
                        case "Выход" : return "qrc:/IconSource/exit.png"
                        }
                    }
                }*/

                        Text {
                text: menuItem2.text
                font.family: parent.font.family
                font.pixelSize: parent.font.pixelSize
                color: menuItem2.highlighted ? "#ffffff" : textColor
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
                //                anchors.verticalCenter: parent.verticalCenter
                //                }
            }

            background: Rectangle {
                implicitWidth: 100
                implicitHeight: fontPixelSize > maxFontPixelSize ? barMaxHeight : barMinHeight
                color: menuItem2.highlighted ? secondaryColor : "transparent"
            }
        }
        title: qsTr("Инструменты")
        Action { text: qsTr("Очистить");onTriggered:fileManager.clearList();}
        Action { text: qsTr("JSon Manager");enabled: false }
//        MenuSeparator {
//            contentItem: Rectangle {
//                implicitHeight: 1
//                color: secondaryColor
//            }
//        }
    }
    Menu{
        font.family: parent.font.family
        font.pixelSize: parent.font.pixelSize
        delegate: MenuItem {
            id: menuItem3
            implicitWidth: 200
            implicitHeight: fontPixelSize > maxFontPixelSize ? barMaxHeight : barMinHeight
            contentItem:/* Row{
                spacing: 2
                Image{
                    sourceSize: Qt.size(20,20)
                    anchors.verticalCenter: parent.verticalCenter
                    source: {
                        switch(textDelegate.text) {
                        case "Открыть" : return "qrc:/IconSource/open.png"
                        case "Сохранить" : return "qrc:/IconSource/save.png"
                        case "Выход" : return "qrc:/IconSource/exit.png"
                        }
                    }
                }*/

                        Text {
                text: menuItem3.text
                font.family: parent.font.family
                font.pixelSize: parent.font.pixelSize
                color: menuItem3.highlighted ? "#ffffff" : textColor
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
                //                anchors.verticalCenter: parent.verticalCenter
                //                }
            }

            background: Rectangle {
                implicitWidth: 100
                implicitHeight: fontPixelSize > maxFontPixelSize ? barMaxHeight : barMinHeight
                color: menuItem3.highlighted ? secondaryColor : "transparent"
            }
        }
        title: qsTr("Помощь")
        Action { text: qsTr("Настройки");onTriggered:   createSettingsPage() }
        Action { text: qsTr("О программе");onTriggered:   createSettingsPage() }
    }

    //for all menu inside MenuBar
    delegate: MenuBarItem {
        id: menuBarItem
        width: menuBarItem.text.width + 10
        contentItem: Text {
            text: menuBarItem.text
            font.family: parent.font
            font.pixelSize: parent.font.pixelSize
            color: menuBarItem.highlighted ? "#ffffff" : textColor
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
        }

        background: Rectangle {

            implicitHeight: fontPixelSize > maxFontPixelSize ? barMaxHeight : barMinHeight
            color: menuBarItem.highlighted ? primaryColor : "transparent"
        }
    }
    background: Rectangle {
        implicitHeight: 25
        color: "#ffffff"
    }
}
