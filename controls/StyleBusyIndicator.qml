import QtQuick 2.0
import QtQuick.Controls 2.14

Column {
    visible: isBusy
    spacing: 5
    BusyIndicator {
        anchors.horizontalCenter: parent.horizontalCenter

    }

    Label {
        text:busyText
        font.family: fontFamily
        font.pixelSize: fontPixelSize - 1
        anchors.horizontalCenter: parent.horizontalCenter
        color: textColor
    }
}
