#include "UserPm.h"

//UserPm::UserPm(QObject *parent) : QObject(parent) { }

UserPm::UserPm(QString userId,QString name,QString phone) : QObject(),
    m_userId(userId),
    m_name(name),
    m_phone(phone)
{
    // иницаиализация вместо setters
}

UserPm::UserPm(QStringList &list)
{
    QListIterator<QString> iter(list);
    while(iter.hasNext()){
        setUserId(iter.next());
        setName(iter.next());
        setPhone(iter.next());
    }
}

UserPm::UserPm(const UserPm &copy): QObject(),
    m_userId(copy.m_userId),
    m_name(copy.m_name),
    m_phone(copy.m_phone)
{

}




QString UserPm::name() const
{
    return m_name;
}

void UserPm::setName(const QString &name)
{
    m_name = name;
}

QString UserPm::phone() const
{
    return m_phone;
}

void UserPm::setPhone(const QString &phone)
{
    m_phone = phone;
}

QString UserPm::userId() const
{
    return m_userId;
}

void UserPm::setUserId(const QString &userId)
{
    m_userId = userId;
}
