#pragma once
#include <QObject>
#include <QString>

class UserPm : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString userId READ userId WRITE setUserId NOTIFY emptySignal)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY emptySignal)
    Q_PROPERTY(QString phone READ phone WRITE setPhone NOTIFY emptySignal)
public:
    //    explicit UserPm(QObject *parent = nullptr);
    UserPm() = default;
    UserPm (QString userId, QString name, QString phone);
    UserPm (QStringList &list);
    UserPm (const UserPm &copy);

    QString userId() const;
    void setUserId(const QString &userId);

    QString name() const;
    void setName(const QString &name);

    QString phone() const;
    void setPhone(const QString &phone);





signals:
    void emptySignal();
private:
    QString m_userId;
    QString m_name;
    QString m_phone;
};
Q_DECLARE_METATYPE(UserPm)

