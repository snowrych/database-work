import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.14

import Simple.Manager 1.0

import "controls"
import "pages"

ApplicationWindow {
    id:window
    property string primaryColor: "#01480f"
    property string secondaryColor: "#027218"
    property string textColor: "#001d06"
    property string borderColor: "#000e03"
    property string fontFamily:"Veranda"
    property real fontPixelSize: 12
    property real maxFontPixelSize:20
    property real barMinHeight: 30
    property real barMaxHeight: 60

    property string busyText : ""
    property bool isBusy: busyText // or other condition
    title: qsTr("Главное окно")

    FileManger{
        id:fileManager
    }
    // old style connections
    //    Connections{
    //        target: fileManager
    //        onUserListChanged:  {
    //              console.log("qml " + fileManager.newList)
    //                  userList.model =  fileManager.newList
    //          }
    //    }
    Connections{
        target: fileManager
        function onBusyTextChanged() {
            console.log( "text: " +fileManager.busyText)
            busyText =  fileManager.busyText
        }
    }
    Connections{
        target: fileManager
        function onUserListChanged() {
            console.log( fileManager.newList)
            userList.model =  fileManager.newList
        }
    }

    visible: true
    width: 640
    height: 480
    menuBar: StyleBar{implicitHeight: fontPixelSize > maxFontPixelSize ? barMaxHeight : barMinHeight}
    StyleBackground{
        id:background
        anchors.fill:parent

        StyleBackground {
            width: parent.width / 2
            height: parent.height /1.5
            clip: true
            ListView{
                id:userList
                anchors {
                    left: parent.left
                    right: parent.right
                    top: parent.top
                    bottom: parent.bottom
                    //rightMargin: 10
                }
                spacing: 0.75
                clip: true
                ScrollBar.vertical:StyleScrollVertical{id:verticalScroll}
                boundsBehavior: Flickable.StopAtBounds
                flickableDirection: Flickable.HorizontalAndVerticalFlick
                //                contentHeight: userList.contentItem.childrenRect.height
                //                contentWidth: userList.contentItem.childrenRect.width

                delegate:   StyleBackground {
                    id:item
                    radius: 3
                    clip: true
                    implicitWidth: parent.width / 2
                    implicitHeight:listColumn.height + 10
                    Column{
                        id:listColumn
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: 5
                        spacing: 2

                        Text{
                            id:textItem1
                            text:"id: "+ modelData.userId
                            font.pixelSize: fontPixelSize - 1
                        }
                        Text{
                            id:textItem2
                            text: "name: "+modelData.name
                            font.pixelSize: fontPixelSize - 1

                        }
                        Text{
                            id:textItem3
                            text: "phone: "+modelData.phone
                            font.pixelSize: fontPixelSize - 1

                        }
                    }
                    MouseArea {
                        id:dragArea
                        anchors.fill: parent
                        cursorShape : Qt.ClosedHandCursor
                        hoverEnabled: true
                        onEntered:  {
                            item.color = secondaryColor
                            textItem1.color = "#ffffff"
                            textItem2.color = "#ffffff"
                            textItem3.color = "#ffffff"
                        }
                        onExited:{
                            item.color ="transparent"
                            textItem1.color = textColor
                            textItem2.color = textColor
                            textItem3.color = textColor
                        }
                    }
                }
            }
            StyleBusyIndicator {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
            }
        }
    }

    function createSettingsPage() {
        var component = Qt.createComponent("qrc:/pages/SettingsPage.qml");
        //        console.log("Component Status:", component.status, component.errorString());
        var pageComponent = component.createObject(window, {"x": window.x + 60, "y": window.y +70});
        pageComponent.show();
    }

}
