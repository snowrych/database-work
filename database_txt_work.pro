QT += quick widgets

CONFIG += c++11

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        DataLayer/FileManager.cpp \
        DataLayer/OfflineDataManager.cpp \
        Core/Options.cpp \
        ViewModels/UserPm.cpp \
        main.cpp

RESOURCES += qml.qrc \
    Sources.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    DataLayer/FileManager.h \
    DataLayer/OfflineDataManager.h \
    Core/Macros.h \
    Core/Options.h \
    ViewModels/UserPm.h

