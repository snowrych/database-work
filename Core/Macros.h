#pragma once
#define SINGLETON(x) static x& instance() { static x s; return s; }
