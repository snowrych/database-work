#pragma once

#include <QObject>
#include "Macros.h"

class Options : public QObject {
    Q_OBJECT

private:
    Options();

public:
    SINGLETON(Options)

//    QString pathToLogs() const;
    QString pathToDatabase() const;
    QString pathToApp() const;
public slots:
//    void updateUrl(const QString &newUrl);

signals:
//    void urlChanged(const QUrl &Addres);

private:

};
