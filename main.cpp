#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQuick>
#include <QQuickView>

#include <QIcon>
#include <QDebug>
#include <QDir>
#include <QVariant>

#include "Core/Options.h"
#include "DataLayer/FileManager.h"
#include "ViewModels/UserPm.h"

int main(int argc, char *argv[])
{
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QQmlApplicationEngine engine;

    QIcon appIcon(":/icons/usrList.svg");
    app.setWindowIcon(appIcon);

    Options& settings = Options::instance();
    QDir().mkpath(settings.pathToApp());
    QDir().mkpath(settings.pathToDatabase());

    qDebug() << "app path: " << settings.pathToApp();
        qmlRegisterType<FileManager>("Simple.Manager",1,0,"FileManger");

    //TODO Add file path TextEdit to qml

    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    return app.exec();
}
