import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.14
import "../controls"

ApplicationWindow {
    id:settingsPage
    StyleBackground{radius:2;anchors.fill: parent}
    title: "Настройки программы"
    width: 500
    height: 360
    Text {
        id:label
        text: "Программа находится на стадии разработки"
        font.family: fontFamily
        font.pixelSize: 20
        anchors{
            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.verticalCenter
        }
    }
}
